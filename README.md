# Image Generation and Inversion 

![](static/Samples.png)

This repository provides reference implementations for GANs that can be trained on  CelebA and MNIST datasets.

There is also an inverter network available for MNIST, which can be trained to generate latent codes for the generator
using image samples. 


## Training the Models

**Dependencies**:
- scipy
- tensorflow
- tensorflow_datasets
- click 

To train on CelebA, place the cropped and aligned CelebA dataset images in `dcgan_celeba/data`. 
To train on MNIST, install the `mnist` package using PyPI or Anaconda.

Once the datasets are available, the models can be trained by invoking `python training.py` in the directory corresponding
to the model you are interested in training. You may optionally specify which GPU/CPU to use for training by adding the 
`--device-pref /device:<your_device>` flag with a tensorflow device specifier.

Alternatively, each model's declaration is isolated in its `model.py` and can be used independently.
