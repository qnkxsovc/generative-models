import tensorflow as tf
import numpy as np

class Generator():
	def __init__(self,
				 embedding_size,
				 learn_rate,
				 batch_size):
		'''
		Generator network model.

		Parameters
		----------
		embedding_size : int
		 dimensionality of the latent codes.
		learn_rate : float
		 learning rate of Adam Optimizer
		batch_size : int
		 size of the input data batches
		'''

		self.batch_size = batch_size
		self.filter_size = (4, 4)
		self.embedding_size = embedding_size
		self.learn_rate = learn_rate

	def build_model(self, inp, is_training, reuse=False):
		'''
		Build the generator model.

		Parameters
		----------
		inp : tf.Operation
		 Input op to the generator model.
		is_training : tf.Operation
		 Boolean placeholder to specify current environment for batch norm.
		reuse : boolean or tf.AUTO_REUSE
		 Should this model reuse variables in the session.
		'''
		with tf.variable_scope("generator", reuse=reuse):
			self.inp = inp

			l1_tconv = tf.layers.Conv2DTranspose(1024, self.filter_size, strides=(1, 1), padding="VALID")
			l2_tconv = tf.layers.Conv2DTranspose(1024, self.filter_size, strides=(2, 2), padding="SAME")
			l3_tconv = tf.layers.Conv2DTranspose(512, self.filter_size, strides=(2, 2), padding="SAME")
			l4_tconv = tf.layers.Conv2DTranspose(256, self.filter_size, strides=(2, 2), padding="SAME")
			l5_tconv = tf.layers.Conv2DTranspose(128, self.filter_size, strides=(2, 2), padding="SAME")
			l6_tconv = tf.layers.Conv2DTranspose(3, self.filter_size, strides=(2, 2), padding="SAME")

			self.l1_normed = tf.layers.batch_normalization(l1_tconv(self.inp), training=is_training)
			self.l1_out = tf.nn.leaky_relu(self.l1_normed)

			self.l2_normed = tf.layers.batch_normalization(l2_tconv(self.l1_out), training=is_training)
			self.l2_out = tf.nn.leaky_relu(self.l2_normed)

			self.l3_normed = tf.layers.batch_normalization(l3_tconv(self.l2_out), training=is_training)
			self.l3_out = tf.nn.leaky_relu(self.l3_normed)

			self.l4_normed = tf.layers.batch_normalization(l4_tconv(self.l3_out), training=is_training)
			self.l4_out = tf.nn.leaky_relu(self.l4_normed)

			self.l5_normed = tf.layers.batch_normalization(l5_tconv(self.l4_out), training=is_training)
			self.l5_out = tf.nn.leaky_relu(self.l5_normed)

			self.final_activations = l6_tconv(self.l5_out)
			self.out = tf.nn.tanh(self.final_activations)

		s_out = tf.summary.image("Generator Output", self.out)
		s_out_mag = tf.summary.histogram("Generator Output", self.out)
		s_out_activ = tf.summary.histogram("Generator Activations", self.final_activations)
		self.summaries = tf.summary.merge([s_out, s_out_mag, s_out_activ])
			
class Discriminator():
	def __init__(self,
				 learn_rate,
				 batch_size):

		self.learn_rate = learn_rate
		self.filter_size = (4, 4)
		self.batch_size = batch_size

	def build_model(self, inp, is_training, reuse=False):
		with tf.variable_scope("discriminator", reuse=reuse):
			self.inp = inp

			l1_conv = tf.layers.Conv2D(128, self.filter_size, strides=(2, 2), padding="SAME")
			l2_conv = tf.layers.Conv2D(256, self.filter_size, strides=(2, 2), padding="SAME")
			l3_conv = tf.layers.Conv2D(512, self.filter_size, strides=(2, 2), padding="SAME")
			l4_conv = tf.layers.Conv2D(1024, self.filter_size, strides=(2, 2), padding="SAME")
			l5_conv = tf.layers.Conv2D(1024, self.filter_size, strides=(2, 2), padding="SAME")
			l6_conv = tf.layers.Conv2D(1, self.filter_size, strides=(1, 1), padding="VALID")

			self.l1_out = tf.nn.leaky_relu(l1_conv(self.inp))

			self.l2_normed = tf.layers.batch_normalization(l2_conv(self.l1_out), training=is_training)
			self.l2_out = tf.nn.leaky_relu(self.l2_normed)

			self.l3_normed = tf.layers.batch_normalization(l3_conv(self.l2_out), training=is_training)
			self.l3_out = tf.nn.leaky_relu(self.l3_normed)

			self.l4_normed = tf.layers.batch_normalization(l4_conv(self.l3_out), training=is_training)
			self.l4_out = tf.nn.leaky_relu(self.l4_normed)

			self.l5_normed = tf.layers.batch_normalization(l5_conv(self.l4_out), training=is_training)
			self.l5_out = tf.nn.leaky_relu(self.l5_normed)

			self.final_activations = l6_conv(self.l5_out)
			self.out = tf.nn.sigmoid(self.final_activations)


		self.summaries = tf.summary.merge([tf.summary.histogram("Discriminator Out", self.out),
										   tf.summary.histogram("Discriminator Final Activ", self.final_activations)])

class DCGAN():
	def __init__(self,
				 embedding_size,
				 learn_rate,
				 batch_size,
				 dataset):

		self.embedding_size = embedding_size
		self.filter_size = (4, 4)
		self.learn_rate = learn_rate
		self.batch_size = batch_size


		self.gen_inp = tf.placeholder(tf.float32, shape=[self.batch_size, 1, 1, self.embedding_size], name="embedding")

		if(dataset):
			self.dataset = dataset
			self.disc_real_inp = dataset.get_next()
		else:
			self.disc_real_inp = tf.placeholder(tf.float32, shape=[self.batch_size, 64, 64, 3], name="real_image")

		self.is_training = tf.placeholder(tf.bool)

		self.g = Generator(embedding_size, learn_rate, batch_size)
		self.g.build_model(self.gen_inp, self.is_training)

		self.d_real = Discriminator(learn_rate, batch_size)
		self.d_real.build_model(self.disc_real_inp, self.is_training)
		self.d_fake = Discriminator(learn_rate, batch_size)
		self.d_fake.build_model(self.g.out, self.is_training, reuse=True)

		self.add_optim_and_loss()

	def add_optim_and_loss(self):
		d_real_logits = self.d_real.final_activations
		d_fake_logits = self.d_fake.final_activations
		self.d_real.loss = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=d_real_logits,
																				  labels=tf.ones([self.batch_size, 1, 1, 1])))
		self.d_fake.loss = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=d_fake_logits,
																				  labels=tf.zeros([self.batch_size, 1, 1, 1])))
		self.g.loss = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=d_fake_logits,
																			 labels=tf.ones([self.batch_size, 1, 1, 1])))
		self.d_net_loss = self.d_real.loss + self.d_fake.loss

		all_vars = tf.trainable_variables()
		d_vars = [x for x in all_vars if x.name.startswith("discriminator")]
		g_vars = [x for x in all_vars if x.name.startswith("generator")]

		self.train_d = tf.train.AdamOptimizer(self.learn_rate, beta1=0.5).minimize(self.d_net_loss, var_list=d_vars)
		self.train_g = tf.train.AdamOptimizer(self.learn_rate, beta1=0.5).minimize(self.g.loss, var_list=g_vars)

