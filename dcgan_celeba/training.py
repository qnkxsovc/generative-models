from model import Generator, Discriminator, DCGAN
import mnist
import tensorflow as tf
import tensorflow_datasets as tfds
from tensorflow.data.experimental import AUTOTUNE
import numpy as np
import os
import shutil
import scipy
import click

def preprocess_img(img):
	img = tf.image.decode_jpeg(img, channels=3)
	img = img[20:-20, :, :] # center crop to a square
	img = tf.image.resize_images(img, [128, 128], align_corners=True)
	mini = tf.reduce_min(img)
	maxi = tf.reduce_max(img)
	return 2 * ( (img - mini) / (maxi - mini) ) - 1

def load_img(path):
	img = tf.read_file(path)
	return preprocess_img(img)

class TrainScheduler():
	def __init__(self,
				 batch_size,
				 log_every,
				 log_dir,
				 checkpoint_dir,
				 device_pref,
				 embedding_size,
				 learn_rate,
				 steps = None,
				 epochs = None):

		'''
		This class wraps a DCGAN network on CelebA and is responsible for batching input data and executing train steps.

		**Note**: You are required to provide the CelebA Cropped and Aligned dataset in the directory `data`.

		Parameters
		----------
		batch_size : int
		 size of the batches
		log_every : int
		 frequency of tensorboard log writes (in iterations)
		log_dir : str
		 directory to write logs
		checkpoint_dir : str
		 directory to save checkpoints
		device_pref : str, optional
		 a tensorflow device specifier to use while training
		embedding_size : int
		 size of the latent embeddings to use in the generator. Must match the size of embeddings in the generator checkpoint.
		learn_rate : float
		 learning rate for Adam Optimizer
		steps : int, optional
		 number of training steps to run. If `None`, then epochs is required.
		epochs : int, optional
		 number of epochs to train the model. If `None`, then steps is required.
		'''

		self.sess = tf.Session(config=tf.ConfigProto(allow_soft_placement=True))

		f_names = [os.path.join("data", v) for v in os.listdir("data")]

		self.data_len = len(f_names)

		path_ds = tf.data.Dataset.from_tensor_slices(f_names)
		image_ds = path_ds.map(load_img, num_parallel_calls=AUTOTUNE)
		self.images = image_ds.repeat().batch(batch_size).prefetch(buffer_size=AUTOTUNE).make_one_shot_iterator()

		self.checkpoint_dir = checkpoint_dir
		self.batch_size = batch_size
		self.log_every = log_every
		self.log_dir = log_dir
		self.embedding_size = embedding_size
		self.learn_rate = learn_rate

		if(device_pref is not None):
			with(tf.device(device_pref)):
				self.dcgan = DCGAN(embedding_size,
								   learn_rate,
								   batch_size,
								   self.images)
		else:
			self.dcgan = DCGAN(embedding_size,
							   learn_rate,
							   batch_size,
							   self.images)

		if (epochs):
			steps = (self.data_len * epochs) // batch_size
		elif (not steps):
			raise ValueError("Steps and epochs cannot both be null")

		self.steps = steps

		self._batch_idx = 0

	def get_batch(self):
		return self.images.take(self.batch_size)

	def latent_sample(self, mean=0, stddev=1):
		return np.random.normal(mean, stddev, (self.batch_size, 1, 1, self.dcgan.g.embedding_size))

	def train(self):

		if(not os.path.exists(self.log_dir)):
			os.mkdir(self.log_dir)
		else:
			shutil.rmtree(self.log_dir)
			os.mkdir(self.log_dir)

		writer = tf.summary.FileWriter(self.log_dir, graph=self.sess.graph)
		saver = tf.train.Saver()

		if(not os.path.exists(self.checkpoint_dir)):
			os.mkdir(self.checkpoint_dir)
			self.sess.run(tf.global_variables_initializer())
		else:
			saver.restore(self.sess, os.path.join(self.checkpoint_dir, "final_model.ckpt"))

		for idx in range(self.steps):
			feed_dict = {
				self.dcgan.g.inp: self.latent_sample(),
				self.dcgan.is_training: True
			}

			_, d_real_loss, d_fake_loss = self.sess.run([self.dcgan.train_d,
														 self.dcgan.d_real.loss,
														 self.dcgan.d_fake.loss],
														feed_dict = feed_dict)
			_, g_loss = self.sess.run([self.dcgan.train_g,
									   self.dcgan.g.loss],
									  feed_dict = feed_dict)

			print("{0}% Trained - Generator loss: {1}, Discriminator loss: {2} (Real) / {3} (Fake)".format(str((100*idx)/self.steps)[:5],
																					   str(g_loss)[:5],
																					   str(d_real_loss)[:5],
																					   str(d_fake_loss)[:5]))

			if(idx % self.log_every == 0):
				logs = self.sess.run(tf.summary.merge([self.dcgan.g.summaries,
													   self.dcgan.d_real.summaries,
													   self.dcgan.d_fake.summaries]),
									 feed_dict=feed_dict)
				writer.add_summary(logs, idx)

		saver.save(self.sess, os.path.join(self.checkpoint_dir, "final_model.ckpt"))

@click.command()
@click.option("--device-pref", default=None, help="device ID of preferred GPU")
def run_training(device_pref):
	scheduler = TrainScheduler(batch_size=100,
							   log_every=1,
							   log_dir = "tensorboard",
							   checkpoint_dir = "checkpoints",
							   device_pref = device_pref,
							   embedding_size=100,
							   learn_rate=0.001,
							   epochs=20)
	scheduler.train()
if __name__ == "__main__":
	run_training()




	