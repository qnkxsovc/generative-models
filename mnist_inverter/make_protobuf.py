from training import TrainScheduler
from model import Inverter
from mnist_dcgan.model import Generator
import tensorflow as tf
import os

graph = tf.Graph()
with graph.as_default():
	input_bytes = tf.placeholder(tf.string, shape=[], name="input_bytes")
	input_bytes = tf.reshape(input_bytes, [])

	input_tensor = tf.image.decode_png(input_bytes, channels=1)
	input_tensor = tf.image.convert_image_dtype(input_tensor, dtype=tf.float32)
	input_tensor = (2 * input_tensor) - 1
	input_tensor = tf.reshape(input_tensor, [1, 64, 64, 1])

	inv = Inverter(100, (4, 4), 0.001, 1)
	inv.build_model(input_tensor, tf.constant(False), reuse=tf.AUTO_REUSE)

	output_tensor = (inv.generator.out + 1)/2
	output_tensor = tf.image.convert_image_dtype(output_tensor, tf.uint8)
	output_tensor = tf.squeeze(output_tensor, [0])

	output_bytes = tf.image.encode_png(output_tensor)
	output_bytes = tf.identity(output_bytes, name="output_bytes")

	gen_variables = [v for v in tf.all_variables() if v.name.startswith("generator")]
	inv_variables = [v for v in tf.all_variables() if v.name.startswith("inverter")]
	saver_gen = tf.train.Saver(gen_variables)
	inverter_gen = tf.train.Saver(inv_variables)

with tf.Session(graph=graph) as sess:
	sess.run(tf.global_variables_initializer())

	saver_gen.restore(sess, os.path.join("mnist_dcgan", "checkpoints", "final_model.ckpt"))
	inverter_gen.restore(sess, os.path.join("checkpoints", "final_model.ckpt"))

	output_graph_def = tf.graph_util.convert_variables_to_constants(sess, graph.as_graph_def(), [output_bytes.op.name])
	tf.train.write_graph(output_graph_def, "protobufs", "model_v1", as_text=False)

builder = tf.saved_model.builder.SavedModelBuilder("serve/1")

with tf.gfile.GFile("protobufs/model_v1", "rb") as protobuf_file:
	graph_def = tf.GraphDef()
	graph_def.ParseFromString(protobuf_file.read())

[inp, out] = tf.import_graph_def(graph_def, name="", return_elements=["input_bytes:0", "output_bytes:0"])

with tf.Session(graph = out.graph) as sess:
	out = tf.expand_dims(out, 0)
	input_bytes = tf.saved_model.utils.build_tensor_info(inp)
	output_bytes = tf.saved_model.utils.build_tensor_info(out)

	signature_definition = tf.saved_model.signature_def_utils.build_signature_def(
		inputs={"input_bytes": input_bytes},
		outputs={"output_bytes": output_bytes},
		method_name=tf.saved_model.signature_constants.PREDICT_METHOD_NAME)

	builder.add_meta_graph_and_variables(
		sess, [tf.saved_model.tag_constants.SERVING],
		signature_def_map={
			tf.saved_model.signature_constants.DEFAULT_SERVING_SIGNATURE_DEF_KEY: signature_definition
		})

builder.save()