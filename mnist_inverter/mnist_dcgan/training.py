from model import Generator, Discriminator, DCGAN
import mnist
import tensorflow as tf
import numpy as np
import os
import shutil
import scipy
import click

class TrainScheduler():
	def __init__(self,
				 batch_size,
				 log_every,
				 log_dir,
				 checkpoint_dir,
				 device_pref,
				 embedding_size,
				 learn_rate,
				 steps = None,
				 epochs = None):
		'''
		This class wraps a DCGAN network on MNIST and is responsible for batching input data and executing train steps.

		Parameters
		----------
		batch_size : int
		 size of the batches
		log_every : int
		 frequency of tensorboard log writes (in iterations)
		log_dir : str
		 directory to write logs
		checkpoint_dir : str
		 directory to save checkpoints
		device_pref : str, optional
		 a tensorflow device specifier to use while training
		embedding_size : int
		 size of the latent embeddings to use in the generator. Must match the size of embeddings in the generator checkpoint.
		learn_rate : float
		 learning rate for Adam Optimizer
		steps : int, optional
		 number of training steps to run. If `None`, then epochs is required.
		epochs : int, optional
		 number of epochs to train the model. If `None`, then steps is required.
		'''

		with tf.Session():
			train_images = tf.image.resize_images(mnist.train_images().reshape(-1, 28, 28, 1), [64, 64]).eval()
			train_images = train_images / (0.5*255) - 1
			self.train_images = train_images

		self.sess = tf.Session(config=tf.ConfigProto(allow_soft_placement=True))

		self.batch_size = batch_size
		self.log_every = log_every
		self.embedding_size = embedding_size
		self.learn_rate = learn_rate

		if(device_pref is not None):
			with(tf.device(device_pref)):
				self.dcgan = DCGAN(embedding_size,
								   learn_rate,
								   batch_size)
		else:
			self.dcgan = DCGAN(embedding_size,
							   learn_rate,
							   batch_size)

		self.saver = tf.train.Saver()


		self.log_dir = log_dir
		
		if(not os.path.exists(self.log_dir)):
			os.mkdir(self.log_dir)
		else:
			shutil.rmtree(self.log_dir)
			os.mkdir(self.log_dir)

		self.writer = tf.summary.FileWriter(self.log_dir, graph=self.sess.graph)

		self.checkpoint_dir = checkpoint_dir
		if(not os.path.exists(self.checkpoint_dir)):
			os.mkdir(self.checkpoint_dir)
			self.restored = False
		else:
			self.saver.restore(self.sess, os.path.join(self.checkpoint_dir, "final_model.ckpt"))
			self.restored = True


		if (epochs):
			steps = (len(self.train_images) * epochs) // batch_size
		elif (not steps):
			raise ValueError("Steps and epochs cannot both be null")

		self.steps = steps

		self._batch_idx = 0

	def get_batch(self):
		idxs = np.arange(self._batch_idx, self._batch_idx + self.batch_size)
		images = np.take(self.train_images, idxs, axis=0, mode="wrap")
		self._batch_idx = (self._batch_idx + self.batch_size) % len(self.train_images)
		return images

	def latent_sample(self, mean=0.0, stddev=1.0):
		return np.random.normal(mean, stddev, (self.batch_size, 1, 1, self.dcgan.g.embedding_size))

	def train(self):
		if(not self.restored):
			self.sess.run(tf.global_variables_initializer())

		for idx in range(self.steps):
			feed_dict = {
				self.dcgan.d_real.inp: self.get_batch(),
				self.dcgan.g.inp: self.latent_sample(stddev=0.25), # this is necessary to clamp latent codes into [-1, 1]
				self.dcgan.is_training: True
			}

			_, d_real_loss, d_fake_loss = self.sess.run([self.dcgan.train_d,
														 self.dcgan.d_real.loss,
														 self.dcgan.d_fake.loss],
														feed_dict = feed_dict)
			_, g_loss = self.sess.run([self.dcgan.train_g,
									   self.dcgan.g.loss],
									  feed_dict = feed_dict)

			print("{0}% Trained - Generator loss: {1}, Discriminator loss: {2} (Real) / {3} (Fake)".format(str((100*idx)/self.steps)[:5],
																					   str(g_loss)[:5],
																					   str(d_real_loss)[:5],
																					   str(d_fake_loss)[:5]))

			if(idx % self.log_every == 0):
				logs = self.sess.run(tf.summary.merge([self.dcgan.g.summaries,
													   self.dcgan.d_real.summaries,
													   self.dcgan.d_fake.summaries]),
									 feed_dict=feed_dict)
				self.writer.add_summary(logs, idx)

		self.saver.save(self.sess, os.path.join(self.checkpoint_dir, "final_model.ckpt"))

@click.command()
@click.option("--device-pref", default=None, help="device ID of preferred GPU")
def run_training(device_pref):
	scheduler = TrainScheduler(batch_size=100,
							   log_every=1,
							   log_dir = "tensorboard",
							   checkpoint_dir = "checkpoints",
							   device_pref = device_pref,
							   embedding_size=100,
							   learn_rate=0.001,
							   epochs=5)
	scheduler.train()
if __name__ == "__main__":
	run_training()




	