from mnist_dcgan.model import DCGAN, Generator, Discriminator
import tensorflow as tf

class Inverter():
	def __init__(self,
				 embedding_size,
				 learn_rate,
				 batch_size):
		'''
		Inverter network model. As the model is built, this will automatically create its own generator, whose weights
		can then be restored from a checkpoint.

		Parameters
		----------
		embedding_size : int
		 dimensionality of the latent codes.
		learn_rate : float
		 learning rate of Adam Optimizer
		batch_size : int
		 size of the input data batches
		'''

		self.batch_size = batch_size
		self.filter_size = (4, 4)
		self.embedding_size = embedding_size
		self.learn_rate = learn_rate


	def build_model(self, inp, is_training, reuse=False):
		'''
		Build the inverter model. As the model is built, this will automatically create its own generator, whose weights
		can then be restored from a checkpoint.

		Parameters
		----------
		inp : tf.Operation
		 Input op to the generator model.
		is_training : tf.Operation
		 Boolean placeholder to specify current environment for batch norm.
		reuse : boolean or tf.AUTO_REUSE
		 Should this model reuse variables in the session.
		'''
		with tf.variable_scope("inverter", reuse=reuse) as scope:
			self.inp = inp
			self.is_training = is_training
			l1_conv = tf.layers.Conv2D(128, self.filter_size, strides=(2, 2), padding="SAME")
			l2_conv = tf.layers.Conv2D(256, self.filter_size, strides=(2, 2), padding="SAME")
			l3_conv = tf.layers.Conv2D(512, self.filter_size, strides=(2, 2), padding="SAME")
			l4_conv = tf.layers.Conv2D(1024, self.filter_size, strides=(2, 2), padding="SAME")
			l5_conv = tf.layers.Conv2D(self.embedding_size, self.filter_size, strides=(1, 1), padding="VALID")

			self.l1_out = tf.nn.leaky_relu(l1_conv(self.inp))

			self.l2_normed = tf.layers.batch_normalization(l2_conv(self.l1_out), training=is_training)
			self.l2_out = tf.nn.leaky_relu(self.l2_normed)

			self.l3_normed = tf.layers.batch_normalization(l3_conv(self.l2_out), training=is_training)
			self.l3_out = tf.nn.leaky_relu(self.l3_normed)

			self.l4_normed = tf.layers.batch_normalization(l4_conv(self.l3_out), training=is_training)
			self.l4_out = tf.nn.leaky_relu(self.l4_normed)

			self.final_activations = l5_conv(self.l4_out)
			self.l5_normed = tf.layers.batch_normalization(self.final_activations, training=is_training)
			self.out = tf.nn.tanh(self.l5_normed)

			self.optimizer = tf.train.AdamOptimizer(self.learn_rate)

		self.generator = Generator(self.embedding_size, self.learn_rate, self.batch_size)
		self.generator.build_model(self.out, tf.constant(False), reuse=True)

		self.loss = tf.losses.mean_squared_error(inp, self.generator.out)
		self.trainable_variables = [v for v in tf.trainable_variables() if v.name.startswith("inverter")]

		with tf.variable_scope(scope, auxiliary_name_scope=False) as v:
			with tf.name_scope(v.original_name_scope):
				self.train = self.optimizer.minimize(self.loss, var_list=self.trainable_variables)


		s_in = tf.summary.image("Inverter Input", self.inp)
		h_in = tf.summary.histogram("Inverter Input", self.inp)
		s_final_activ = tf.summary.histogram("Inverter Output Activations", self.final_activations)
		s_out = tf.summary.image("Generator Output", self.generator.out)
		inv = tf.summary.histogram("Inversion", self.out)
		layer_activations = tf.summary.merge([
			tf.summary.histogram("l1_out", self.l1_out),
			tf.summary.histogram("l2_out", self.l2_out),
			tf.summary.histogram("l3_out", self.l3_out),
			tf.summary.histogram("l4_out", self.l4_out),
			tf.summary.histogram("bn_l5_out", self.l5_normed)
		])

		self.summaries = tf.summary.merge([s_out, s_in, inv, h_in, s_final_activ, layer_activations])

