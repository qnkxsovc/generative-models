import tensorflow as tf
import mnist
import mnist_dcgan.training
import os
import shutil
from model import Inverter
import numpy as np
import click

class TrainScheduler():
	def __init__(self,
				 batch_size,
				 log_every,
				 log_dir,
				 checkpoint_dir,
				 device_pref,
				 embedding_size,
				 learn_rate,
				 steps=None,
				 epochs=None):
		'''
		This class wraps an inverter network and is responsible for batching input data and executing train steps.

		**Note**: You are required to provide a checkpoint for a trained DCGAN at the directory `mnist_dcgan/checkpoints`.
		This checkpoint will be used to train the inverter.

		Parameters
		----------
		batch_size : int
		 size of the batches
		log_every : int
		 frequency of tensorboard log writes (in iterations)
		log_dir : str
		 directory to write logs
		checkpoint_dir : str
		 directory to save checkpoints
		device_pref : str, optional
		 a tensorflow device specifier to use while training
		embedding_size : int
		 size of the latent embeddings to use in the generator. Must match the size of embeddings in the generator checkpoint.
		learn_rate : float
		 learning rate for Adam Optimizer
		steps : int, optional
		 number of training steps to run. If `None`, then epochs is required.
		epochs : int, optional
		 number of epochs to train the model. If `None`, then steps is required.
		'''

		with tf.Session():
			train_images = tf.image.resize_images(mnist.train_images().reshape(-1, 28, 28, 1), [64, 64]).eval()
			train_images = train_images / (0.5 * 255) - 1
			self.train_images = train_images

		self.trained_dcgan = mnist_dcgan.training.TrainScheduler(batch_size,
																 log_every,
																 "mnist_dcgan/tensorboard/",
																 "mnist_dcgan/checkpoints",
																 device_pref,
																 embedding_size,
																 learn_rate,
																 steps,
																 epochs)

		self.sess = self.trained_dcgan.sess # share the same session

		self.batch_size = batch_size
		self.log_every = log_every
		self.embedding_size = embedding_size
		self.learn_rate = learn_rate

		# both of these get copied by build_model() to be attributes of the Inverter
		self.inverter_input = tf.placeholder(tf.float32, (batch_size, 64, 64, 1))
		self.is_training = tf.placeholder(tf.bool)

		if (device_pref is not None):
			with(tf.device(device_pref)):
				self.inverter = Inverter(self.trained_dcgan.dcgan.embedding_size,
										 self.trained_dcgan.dcgan.learn_rate,
										 self.trained_dcgan.dcgan.batch_size)
				self.inverter.build_model(self.inverter_input, self.is_training)
		else:
			self.inverter = Inverter(self.trained_dcgan.dcgan.embedding_size,
									 self.trained_dcgan.dcgan.learn_rate,
									 self.trained_dcgan.dcgan.batch_size)
			self.inverter.build_model(self.inverter_input, self.is_training)

		self.saver = tf.train.Saver()

		self.log_dir = log_dir

		if (not os.path.exists(self.log_dir)):
			os.mkdir(self.log_dir)
		else:
			shutil.rmtree(self.log_dir)
			os.mkdir(self.log_dir)

		self.writer = tf.summary.FileWriter(self.log_dir, graph=self.sess.graph)

		self.checkpoint_dir = checkpoint_dir
		if (not os.path.exists(self.checkpoint_dir)):
			os.mkdir(self.checkpoint_dir)

		if (epochs):
			steps = (len(self.train_images) * epochs) // batch_size
		elif (not steps):
			raise ValueError("Steps and epochs cannot both be null")

		self.steps = steps

		self._batch_idx = 0

	def get_batch(self):
		idxs = np.arange(self._batch_idx, self._batch_idx + self.batch_size)
		images = np.take(self.train_images, idxs, axis=0, mode="wrap")
		self._batch_idx = (self._batch_idx + self.batch_size) % len(self.train_images)
		return images

	def train(self):
		uninit_vars = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope='inverter')
		self.sess.run(tf.initializers.variables(uninit_vars))

		for idx in range(self.steps):
			feed_dict = {
				self.inverter.inp: self.get_batch(),
				self.inverter.is_training: True
			}

			_, inv_loss = self.sess.run([self.inverter.train, self.inverter.loss], feed_dict=feed_dict)

			print("{0}% Trained - Inverter loss: {1}".format(
				str((100 * idx) / self.steps)[:5],
				str(inv_loss)[:5]))

			if (idx % self.log_every == 0):
				logs = self.sess.run(tf.summary.merge([self.inverter.summaries]),
									 feed_dict=feed_dict)
				self.writer.add_summary(logs, idx)

		self.saver.save(self.sess, os.path.join(self.checkpoint_dir, "final_model.ckpt"))


@click.command()
@click.option("--device-pref", default=None, help="device ID of preferred GPU")
def run_training(device_pref):
	scheduler = TrainScheduler(batch_size=100,
							   log_every=1,
							   log_dir="tensorboard",
							   checkpoint_dir="checkpoints",
							   device_pref=device_pref,
							   embedding_size=100,
							   learn_rate=0.001,
							   epochs=5)
	scheduler.train()


if __name__ == "__main__":
	run_training()
