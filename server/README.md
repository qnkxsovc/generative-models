# MNIST Inverter Demo Server

The demo server interface is implemented in pure HTML/CSS/Javascript, which is all contained in 
[index.html](https://gitlab.com/qnkxsovc/generative-models/blob/master/server/index.html). 
It is designed to query the inverter through Tensorfow's Model Serving API, which is documented [here](https://www.tensorflow.org/tfx/serving/serving_basic).

The code for compiling the inverter into a protobuf can be found in [mnist_inverter/make_protobuf.py](https://gitlab.com/qnkxsovc/generative-models/blob/master/mnist_inverter/make_protobuf.py).

A runnable Docker container is also available. You can run your own demo server on `http://localhost` with the command `docker run -d -p 80:80 qnkxsovc/digits-demo`.


*Credits to resources [[1]](https://medium.com/@tmlabonte/serving-image-based-deep-learning-models-with-tensorflow-servings-restful-api-d365c16a7dc4)
 and on [[2]](https://zipso.net/a-simple-touchscreen-sketchpad-using-javascript-and-html5/) for information on building the demo.*

